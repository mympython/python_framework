import sys
from openpyxl import Workbook, load_workbook
from openpyxl.styles import Alignment, Font
from openpyxl.styles.fills import PatternFill
from openpyxl.styles.borders import Border, Side
import random, string
import os.path
import re


#######################################################
#################### USAGE ############################
#######################################################
#
#On the command line run:
#
#result_reporter.py [path_to_pretty_output]
#
#pretty.output is by default saved in \behave_outputs
#
#######################################################
#######################################################


ARG = sys.argv[1]
sheet_name = ARG.split('.')[-2]

c = 0
delete_sheet = False

### Check if the spreadsheet exists. If not, create it.

if os.path.isfile('ex.xlsx'):
    wb = load_workbook('ex.xlsx')
else:
    wb = Workbook()
    delete_sheet = True

### Add a new sheet to the spreadsheet

sheet = 'test' + ''.join(random.choice(string.ascii_letters) for m in range(6))
wb.create_sheet(sheet_name)
ws = wb[sheet_name]
if delete_sheet:
    wb.remove(wb['Sheet'])

### Align text in columns A and B

for row in ws.iter_rows(min_row=1, max_col=2, max_row=100):
    for cell in row:
        cell.alignment = Alignment(vertical='center', horizontal='center')

### Expand row C for better readability

for row in range(0, 100):
    ws.row_dimensions[row].height = 45

### Wrap text in column C (multiline strings)

for row in ws.iter_rows(min_row=1, min_col=3, max_col=3, max_row=100):
    for cell in row:
        cell.alignment = Alignment(wrap_text=True)

### Resize columns

ws.column_dimensions['A'].width = 13.0
ws.column_dimensions['B'].width = 10.0
ws.column_dimensions['C'].width = 200.0

### Create fills

beigeFill = PatternFill(start_color='FDFEE8', end_color='FDFEE8', fill_type='solid')
greenFill = PatternFill(start_color='00FF00', end_color='00FF00', fill_type='solid')
redFill = PatternFill(start_color='FF0000', end_color='FF0000', fill_type='solid')

### Create border options

thin_border = Border(left=Side(style='thin'),
                     right=Side(style='thin'),
                     top=Side(style='thin'),
                     bottom=Side(style='thin'))

thick_border = Border(left=Side(style='thick'),
                     right=Side(style='thick'),
                     top=Side(style='thick'),
                     bottom=Side(style='thick'))

row = 2

ws.cell(column=1, row=1).value = "Test Case"
ws.cell(column=1, row=1).border = thick_border
ws.cell(column=1, row=1).font = Font(size='12', bold=True)
ws.cell(column=1, row=1).fill = beigeFill

ws.cell(column=2, row=1).value = "Result"
ws.cell(column=2, row=1).border = thick_border
ws.cell(column=2, row=1).font = Font(size='12', bold=True)
ws.cell(column=2, row=1).fill = beigeFill

ws.cell(column=3, row=1).value = "Traceback"
ws.cell(column=3, row=1).border = thick_border
ws.cell(column=3, row=1).font = Font(size='12', bold=True)
ws.cell(column=3, row=1).alignment = Alignment(vertical='center', horizontal='center')
ws.cell(column=3, row=1).fill = beigeFill

text = []
with open(ARG, 'r') as file:
    inp = file.readlines()

for line in inp:
    text.append(line.strip())

text[:] = [item for item in text if item != '']
text.append("END_FILE")


### Get from the input only the scenario tag, result, and traceback

output = []
output_c = 0

for a in range(0, len(text)):
    if "@MVP" in text[a]:
        output.append([text[a]])
        for b in range(a, len(text)):
            if "Traceback" in text[b] or "Assertion" in text[b]:
                msg = ''
                msgs = []
                for c in range(b, len(text)):
                    if "@MVP" not in text[c] and "# None" not in text[c]:
                        output[output_c].insert(1, 'FAIL')
                        msgs.append(text[c])
                    else:
                        break
                for elem in msgs:
                    msg += elem
                output[output_c].insert(2, msg)
                output_c += 1
                break
            if "Feature" in text[b+1] or "@MVP" in text[b+1] or "END_FILE" in text[b+1]:
                output[output_c].insert(1, 'PASS')
                output_c += 1
                break

output_two = []
for aa in range(0, len(output)):
    output[aa] = output[aa][0:3]


with open('sorter.txt', 'w') as srt:
    for elem in output:
        for subelem in elem:
            srt.write(str(subelem) + ',')
        srt.write('\n')

with open('sorter.txt', 'r') as dsrt:
    output_two = dsrt.readlines()

os.remove('sorter.txt')

pattern = re.compile('@MVP-(\d+)(\d+)')

result = sorted(output_two, key=lambda s: tuple(map(int, pattern.search(s).groups())))

result = [item.strip() for item in result]

for a in range(0, len(result)):
    link = result[a].split(',')[0].replace('@', '')
    ws.cell(column=1, row=row).value = f"=HYPERLINK(\"https://universityofmanchester.atlassian.net/browse/{link}\", \"{link}\")"
    ws.cell(column=1, row=row).font = Font(color='334FFF', underline='single')

    ws.cell(column=2, row=row).value = result[a].split(',')[1]
    if "PASS" in result[a].split(',')[1]:
        ws.cell(column=2, row=row).fill = greenFill
    if "FAIL" in result[a].split(',')[1]:
        ws.cell(column=2, row=row).fill = redFill
    ws.cell(column=2, row=row).border = thin_border

    if len(result[a].split(',')[0:-1]) > 2:
        ws.cell(column=3, row=row).value = str(result[a].split(',')[2:])

    row += 1

wb.save('ex.xlsx')
