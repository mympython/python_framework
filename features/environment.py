from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
import os
import datetime
from shutil import move
from time import sleep
from threading import Thread


__TIMEOUT = 10

def choose_browser(context, browser, mode=None):
	if "original browser" in browser:
		browser = context.config.userdata['browser']
	if "chrome" in browser:
		if mode is "incognito":
			chrome_options = webdriver.ChromeOptions()
			chrome_options.add_argument("--incognito")
			context.driver = webdriver.Chrome(f"{os.getcwd()}\\drivers\\chromedriver.exe",
											  chrome_options=chrome_options)
		else:
			context.driver = webdriver.Chrome(f"{os.getcwd()}\\drivers\\chromedriver.exe")
	elif "firefox" in browser:
		if mode is "incognito":
			firefox_profile = webdriver.FirefoxProfile()
			firefox_profile.set_preference("browser.privatebrowsing.autostart", True)
			context.driver = webdriver.Firefox(executable_path=f"{os.getcwd()}\\drivers\\geckodriver.exe",
											   firefox_profile=firefox_profile)
		else:
			context.driver = webdriver.Firefox(executable_path=f"{os.getcwd()}\\drivers\\geckodriver.exe")
	elif "sauce" in browser:
		sauce_username = context.config.userdata.get("username")
		sauce_access_key = context.config.userdata.get("userkey")
		remote_url = "https://ondemand.saucelabs.com:443/wd/hub"

		sauceOptions = {
			"screenResolution": "1280x768",
			"seleniumVersion": "3.13.0",
			'build': "Onboarding Sample App - Python",
			'name': context.scenario.name,
			"username": sauce_username,
			"accessKey": sauce_access_key,
			# best practices involve setting a build number for version control
			"build": "build-0.0.1",
			# tags to filter test reporting.
			"tags": ['instant-sauce', 'ruby-rspec', 'module4'],
			# setting sauce-runner specific parameters such as timeouts helps
			# manage test execution speed.
			"maxDuration": 1800,
			"commandTimeout": 300,
			"idleTimeout": 1000
		}

		chromeOpts = {
			'platformName': "Windows 10",
			'browserName': "chrome",
			'browserVersion': '71.0',
			'goog:chromeOptions': {'w3c': True},
			'sauce:options': sauceOptions
		}
		context.driver = webdriver.Remote(remote_url, desired_capabilities=chromeOpts)

def pretty_output():
	now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M").replace("-", "_").replace(" ", "__").replace(":", "_")
	if os.path.exists("pretty.output"):
		if not os.path.exists("behave_outputs"):
			os.makedirs("behave_outputs")
		counter = 0
		while os.path.exists('pretty.output'):
			try:
				sleep(0.5)
				counter += 1
				if counter > 50:
					print("Took to long to wait for the output to be created!")
					break
				move('pretty.output', 'behave_outputs/pretty.output')
				if os.path.exists(f'behave_outputs/pretty.output.{now}.txt'):
					os.rename('behave_outputs/pretty.output', f'behave_outputs/pretty.output.{now}_2.txt')
				else:
					os.rename('behave_outputs/pretty.output', f'behave_outputs/pretty.output.{now}.txt')
			except PermissionError:
				pass

# def before_all(context):
def before_scenario(context, scenario):
	context.test_output = open('test_output.txt', 'a')
	browser = context.config.userdata.get("browser")
	choose_browser(context, browser)
	context.driver.maximize_window()
	context.wait = WebDriverWait(context.driver, __TIMEOUT)

def after_scenario(context, scenario):
	context.test_output.close()
	context.driver.quit()

def after_all(context):
	Thread(target=pretty_output).start()
