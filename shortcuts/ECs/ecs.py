from selenium.webdriver.support import expected_conditions as EC


def presence(context, *locator):
    return context.wait.until(EC.presence_of_element_located((*locator)))


def invisibility(context, *locator):
    return context.wait.until(EC.invisibility_of_element_located((*locator)))


def visibility(context, *locator):
    return context.wait.until(EC.visibility_of_element_located((*locator)))

def clickable(context, *locator):
	return context.wait.until(EC.element_to_be_clickable(*locator))
