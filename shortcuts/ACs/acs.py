from selenium.webdriver.common.action_chains import ActionChains as AC
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from selenium.webdriver.common.by import By
from time import sleep


def double_click(context):
	return AC(context.driver).double_click().perform()
	
# Executes Javascript at intervals to check a condtion

def JS_wait_for_condition(context, time_step, condition):
	for x in range(1, 10):
		x = context.driver.execute_script(condition)
		if(x):
			return
		sleep(time_step)

def move_to_element(context, element):
	return AC(context.driver).move_to_element(element).perform()

def send_keys(context, text):
	return AC(context.driver).send_keys(text).perform()

def switch_to_iframe(context, *locator):
	assert context.wait.until(EC.presence_of_element_located(locator)), "The iframe doesn't exist"
	context.driver.switch_to.frame(context.driver.find_element(*locator))

def switch_to_iframe_by_id(context, id):
	assert context.wait.until(EC.presence_of_element_located((By.ID, id))), "The iframe doesn't exist"
	context.driver.switch_to.frame(id)

