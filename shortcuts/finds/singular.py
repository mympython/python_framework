from selenium import webdriver
from selenium.webdriver.common.by import By


def find_elem(context, *locator):
    return context.driver.find_element(*locator)

def findXP(context, xpath):
    return context.driver.find_element_by_xpath(xpath)
