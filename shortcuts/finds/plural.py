from selenium import webdriver
from selenium.webdriver.common.by import By


def find_elems(context, *locator):
    return context.driver.find_elements(*locator)

def findXPs(context, xpath):
    return context.driver.find_elements_by_xpath(xpath)
