from shutil import (copy,
	move)
import os
import glob
import sys
from time import sleep
import time
import math
from threading import Thread


start_time = time.time()

def print_help():
	print("""\n\t\tUsage: runner.py [browser] [number of parallel sessions]
			 \n\t\t[browser]:
			 \n\t\t\tchrome   --  start chromedriver
			 \n\t\t\tfirefox  --  start geckodriver
			 \n\t\t\tsauce    --  execute tests on Saucelabs (edit environment.py for OS and browser)
			 \n\t\t[number of parallel sessions]:
			 \n\t\t\t1 or higher""")

# Read arguments from the command line
ARG1 = ""
ARG2 = ""

try:
	ARG1 = sys.argv[1]
	ARG2 = int(sys.argv[2])
except IndexError:
	print_help()
	sys.exit(1)

# Create the required folders if they don't exist
if not os.path.exists("temp_output"):
	os.makedirs("temp_output")
if not os.path.exists("html_output"):
	os.makedirs("html_output")

# Find and delete all files in /temp_output, /html_output
FILES = glob.glob('temp_output/*') + glob.glob('html_output/*')
try:
	for f in FILES:
		os.remove(f)
except FileNotFoundError:
	pass

# Delete behave.ini if exists
try:
	os.remove("behave.ini")
except FileNotFoundError:
	pass

# Delete test_output.txt if exists
try:
	os.remove("test_output.txt")
except FileNotFoundError:
	pass

# Find all .feature files and store them in 2D lists
SCENS = []

for dirpath, dirnames, filenames in os.walk("./features"):
	for filename in [f for f in filenames if f.endswith(".feature")]:
		with open(f"{dirpath}/{filename}", 'r') as fin:
			lines = fin.readlines()
			for line in lines:
				if "Scenario" in line:
					SCENS.append(line.split("Scenario: ")[1].strip("\n"))

# Copy the selected .ini as behave.ini so that behave has an .ini file to take parameters from
try:
	if 'chrome' in ARG1:
		copy("chrome.ini", "behave.ini")
	elif 'firefox' in ARG1:
		copy("firefox.ini", "behave.ini")
	elif 'sauce' in ARG1:
		copy("sauce.ini", "behave.ini")
	elif not ARG2.is_integer() or not ARG2 or not 'chrome' or not 'firefox' in ARG1:
		print_help()
		sys.exit(1)
except AttributeError:
	print_help()
	sys.exit(1)

# Fire up a number of behave processes equal to the number of features
def start_run(parallel):
	print()
	cc = 0
	if not os.path.exists("temp_output"):
		os.makedirs("temp_output")
	if not os.path.exists("html_output"):
		os.makedirs("html_output")
	while not len(glob.glob("html_output/*.txt")) == len(SCENS):
		while len(glob.glob("temp_output/*.txt")) < parallel:
			if len(glob.glob("html_output/*.txt")) == len(SCENS):
				break
			if cc + 1 <= len(SCENS):
				output = f"temp_output/\"{cc}\".txt"
				os.system(f"start /b behave -n \"{SCENS[cc]}\" > {output}")
				print(f"\t\tTesting scenario: {SCENS[cc]}")
				Thread(target=move_files, args=(cc,)).start()
				cc += 1

def move_files(cc):
	print(f"\nProcessing: temp_output/{cc}.txt\n")
	while True:
		f_open = open(f"temp_output/{cc}.txt")
		f_read = f_open.read()
		f_open.close()
		sleep(1)
		if "Took" in f_read:
			move(f"temp_output/{cc}.txt",
				 f"html_output/{cc}.txt")
			break


if ARG2 < 1:
	print("\n\tERROR!")
	print(f"\n\t\tNumber of parallel sessions must be an integer 1 or higher! Is --> \
		'{sys.argv[2]}', '{type(sys.argv[2])}'")
	sys.exit(1)

try:
	start_run(ARG2)
except (TypeError, ValueError) as e:
	print("\n\tERROR!")
	print(f"\n\t\tNumber of parallel sessions must be an integer 1 or higher! Is --> \
		'{ARG2}', '{type(ARG2)}'")
	sys.exit(1)

duration_s = math.ceil(time.time() - start_time)
duration_m = 0
while duration_s > 60:
	duration_m += 1
	duration_s -= 60

if duration_m > 0:
	with open("html_output/execution_duration.txt", "w") as dur:
		dur.write(f"Total duration: {duration_m}m{duration_s}s")
else:
	with open("html_output/execution_duration.txt", "w") as dur:
		dur.write(f"Total duration: {duration_s}s")

